<?php
/**
 * Copyright (C) 2018 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\ZendPsr18Bridge;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Request\Serializer;
use Zend\Http\Client as ZendClient;
use Zend\Http\Exception\ExceptionInterface as ZendException;
use Zend\Http\Request as ZendRequest;
use Zend\Psr7Bridge\Psr7Response;

class Client implements ClientInterface
{
    /**
     * @var ZendClient
     */
    protected $zendClient;

    public function __construct(ZendClient $zendClient)
    {
        $this->zendClient = $zendClient;
    }

    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws ClientException
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $serializedRequest = Serializer::toString($request);
        $zendRequest = ZendRequest::fromString($serializedRequest);
        $requestHost = ($requestUri = $request->getUri())->getHost();
        ($zendRequestUri = $zendRequest->getUri())->setHost($requestHost);
        $zendRequestUri->setScheme($requestUri->getScheme());

        try {
            $zendResponse = $this->zendClient->send($zendRequest);
        } catch (ZendException $exception) {
            throw new ClientException($exception->getMessage(), null,
                $exception);
        }

        $this->zendClient->resetParameters(true);

        return Psr7Response::fromZend($zendResponse);
    }
}
